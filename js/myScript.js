          var canvas; 
          var context;
          var markerWidth = 5;
          var str = "sans-serif";
          
          var dataURL;
          var initial_x;                  //Clearly you know what this is.
          var initial_y;
          var mouse_pressed = false;
          var paint_type = 0;

          var HistoryArr = new Array();  //for Redo and Undo
          var step = -1;                 //steps of redo and undo

          var redVal = 0;
          var blueVal = 0;
          var greenVal = 0;

          
          function initialize(){
            initial();
          }

          function Line(){
              paint_type = 0;  
              context.strokeStyle = "rgb(" + redVal + "," + greenVal + "," + blueVal + ")";
              canvas.style.cursor= "crosshair";
          }

          function Rec(){
              paint_type = 1;
              canvas.style.cursor= "nw-resize";
          }

          function Circle(){
              paint_type = 2;
              canvas.style.cursor= "wait";
          }

          function Tri(){
            paint_type = 5;
            canvas.style.cursor= "no-drop";
          }
          
          function Text(){
            paint_type = 3;
            canvas.style.cursor= "grab";
          }
    
          function initial(){
            canvas = document.getElementById("myCanvas");
            context = canvas.getContext("2d");
            canvas.addEventListener("mousemove",mouse_move,false);
            canvas.addEventListener("mouseup",mouse_up,false);
            canvas.addEventListener("mousedown",mouse_down,false);
            document.getElementById("fileUpload").addEventListener("change",readImage,false);
            canvas.style.cursor= "crosshair";
            context.fillStyle = "white";
            context.lineCap = 'round';
            context.fillRect(0, 0, canvas.width, canvas.height);
            HistoryArr.push(canvas.toDataURL());
            step++;
            context.fillStyle = "blue";
          }
    
          function mouse_move(event){
            if(!mouse_pressed){
              return false;
            }
            var x = 0;
            var y = 0;
            x = event.offsetX;
            y = event.offsetY;
            context.lineWidth=markerWidth;
            if(paint_type==0){
              context.lineTo(x,y);
              context.stroke();
            }else if(paint_type==4){  
              context.strokeStyle = '#ffffff';
              context.lineTo(x,y);
              context.stroke();
            }
          }
    
          function mouse_down(event){
            var x = 0;
            var y = 0;
            x = event.offsetX;
            y = event.offsetY;
            initial_x = event.offsetX;
            initial_y = event.offsetY;
            mouse_pressed = true;
            context.moveTo(x,y);
            context.beginPath();
          }
          
          function mouse_up(event){
            var x = 0;
            var y = 0;
            x = event.offsetX;
            y = event.offsetY;
            var delta_x = x - initial_x;
            var delta_y = y - initial_y;
            mouse_pressed = false;

            if(paint_type == 0){
                context.lineTo(x,y);
            }
            else if(paint_type==1){
              context.lineWidth=3;
              context.strokeStyle = "black";
              context.rect( initial_x, initial_y, delta_x, delta_y);
              context.fill();
            }
            else if(paint_type==2){              
              context.lineWidth=3;
              context.strokeStyle = "black";
              context.arc( initial_x + delta_x/2, initial_y + delta_y/2, Math.sqrt(Math.pow(delta_x,2) + Math.pow(delta_y,2)) , 0*Math.PI, 2*Math.PI);
              context.fill();
            }
            else if(paint_type==3){
              context.font = (markerWidth+5) + 'px ' + str;//"50px Arial";
              context.fillText(document.getElementById("textbar").value,x,y);
              console.log(context.font);
              console.log(markerWidth);
            }
            else if(paint_type==4){  
              context.strokeStyle = '#ffffff';
              context.lineTo(x,y);
            }
            else if(paint_type==5){
              context.lineWidth=3;
              context.moveTo(initial_x, initial_y);
              context.lineTo(x, initial_y);
              context.lineTo(x, y);
              context.lineTo(initial_x, initial_y);
              context.strokeStyle = "black";
              context.fill();
              context.stroke();
              console.log(initial_x,initial_y,x,y);
            }

            context.stroke();
            context.closePath();
            context.lineWidth=markerWidth;

            step++;
            if (step < HistoryArr.length) { HistoryArr.length = step; }
            HistoryArr.push(canvas.toDataURL());
          }
    
          function Reset(){
            context.clearRect(0, 0, context.canvas.width, context.canvas.height);
            context.fillStyle = "white";
            context.fillRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = "rgb(" + redVal + "," + greenVal + "," + blueVal + ")";
            step = -1;
          }

          function download(){
            var link = document.createElement('a');
            link.download = "test.png";
            link.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");;
            link.click();
          }

          function Undo() {
              if (step > 0) {
                  step--;
                  var canvasPic = new Image();
                  canvasPic.src = HistoryArr[step];
                  
                  canvasPic.onload = function () {
                    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
                     context.drawImage(canvasPic, 0, 0); }
              }
          }

          function Redo() {
              if (step < HistoryArr.length-1) {
                  step++;
                  var canvasPic = new Image();
                  canvasPic.src = HistoryArr[step];
                  canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
              }
          }

          function Erase(){
            paint_type = 4;
            canvas.style.cursor= "cell";
          }

          function markerWid(){  
            markerWidth = document.getElementById("marker").value;
            console.log(document.getElementById("marker").value);
          }

          function readImage(){
            if(this.files&&this.files[0]){
              context.clearRect(0, 0, context.canvas.width, context.canvas.height);
              var FR = new FileReader();
              FR.onload = function(event) {
                var img = new Image();
                img.addEventListener("load", function() {
                  context.drawImage(img, 0, 0,canvas.width,canvas.height);
                });
                img.src = event.target.result;
              };       
              FR.readAsDataURL( this.files[0] );
            }
          }

          function ChangeColor(){
            redVal = document.getElementById("redMark").value;
            blueVal = document.getElementById("blueMark").value;
            greenVal = document.getElementById("greenMark").value;
            document.getElementById("showColor").style.backgroundColor = "rgb(" + redVal + "," + greenVal + "," + blueVal + ")";
            context.strokeStyle = "rgb(" + redVal + "," + greenVal + "," + blueVal + ")";
            context.fillStyle = "rgb(" + redVal + "," + greenVal + "," + blueVal + ")";
            
          }

          function fontAct(){
            str = document.getElementById("fontFam").value;
          }
          
          window.onload = initialize;