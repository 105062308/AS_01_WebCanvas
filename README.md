# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

105062308 朱祐葳  ReadMe
<-----------HTML主要架構------------>

在Head部分，引入css檔以及js檔。

*************調色盤**************
********口口口口口口口口口********
********口口口口口口口口口********
********口口口口口口口口口********
********口口口口口口口口口********
********口口口口口口口口口********
************Buttons**************
************Width Bar************
***********Text Bar**************
*********Upload Image************

以上是主要的架構，調色盤利用table，代表RGB三個顏色
而調色盤下面有三個Input作為RGB三種顏色的數值
緊接著是Canvas，最下面有一整列button可供選擇不同的功能
最下面則是筆刷粗細、字型選擇、打字欄位以及上傳圖案



<----------------css------------------->

CSS主要美化了使用介面

1.加上背景圖案
2.將版面置中，看起來更為舒適
3.利用現成的CSS Style，將button以及input range看起來更圓滑
4.調整間距以及div的排列，使用起來較順暢


<-----------------js------------------->

這次js的重點主要是利用canvas的x,y座標，以及自己設的變數paint_type完成

在initial時，使用addEventListener，增加點集偵測
此外，也將筆刷大小、顏色等相關的變數設置完成

利用button的Onclick呼叫function

1.Rectangle
2.Circle
3.Triangle
4.Eraser
5.Text

這幾個不同的功能，每當被按下時，會將參數改變成自己的type

主要改變：

1.將paint_type改變成屬於自己的數字
2.改變Crusor的樣式
3.必要時改變邊框顏色、粗細

最主要的事情都還是在mouse_up的時候完成
利用自己設定的變數initial_x和initial_y
以及最後mouse_up時的x,y
再配合canvas的script，即可以畫出各樣的圖形

--------------Clear----------------

利用Canvas的clearRect，將整個畫布清除
清除之後，再將畫布填滿白色


--------------Undo and Redo------------

這兩個功能，是利用了一個Stack以及Stack的index來完成
每當畫完一張圖，就會將Canvas的圖案利用toDataURL存入stack
此時index也會++

每當需要Redo或是Undo時，就利用index，將stack裡面的圖案取出
並且畫在Canvas上，即可以完成此項功能


-----------畫筆粗細以及RGB調整----------

利用HTML的標籤─Input，再利用onchange呼叫function
每當輸入的值一改變，就會將value套用在linwidth、strokeStyle上

實際的改變，則是利用js特殊的number + string會輸出一個string
來改變字體的font，抑或是改變粗細、顏色


----------Upload and Download----------

Upload的方法和Redo以及Undo有點相似
先將檔案利用FileReader以及Image的object讀入
如果成功load，就會將上傳的圖片畫在畫布上填滿

而Downlaod利用HTML的標籤<a>有一個download 的 attribute
先利用toDataURL將Canvas的圖片載入，再Click後即可以下載


